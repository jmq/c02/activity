const Product = require('../models/Product')

module.exports.addProduct = (reqBody) => {	
	let newProduct =  new Product ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	})
	return newProduct.save().then((product, error) => {
		if(error) {
			return false
		} else {
			return product
		}
	})
}


//getAllProduct
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
}


//Retrieve Acttive products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {		
		return result
	})
}

//Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.id).then(result => {
		return result
	})
}


//Update product
// module.exports.updateProduct = (data) => {	
// 	return Product.findById(data.courseId).then((result, error) => {		
// 		if(data.isAdmin) {
// 			result.name = data.updatedCourse.name
// 			result.description = data.updatedCourse.description
// 			result.price = data.updatedCourse.price
// 			result.isActive = data.updatedCourse.isActive

// 			console.log(result)
// 			return result.save().then((updatedCourse, error) => {
// 				if(error) {
// 					return false 					
// 				} else {
// 					return updatedCourse
// 				}
// 			})
// 		} else {
// 			return 'Not Admin.'
// 		}
// 	})
// }


//Update product XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
module.exports.updateProduct = (data) => {	
	return Product.findById(data.productId).then((result, error) => {		
		result.name = data.updatedProduct.name
		result.description = data.updatedProduct.description
		result.price = data.updatedProduct.price
		//result.isActive = data.updatedProduct.isActive
		
		if(data.isAdmin){
			return result.save().then((updatedProduct, error) => {
			if(error) {
				return false 					
			} else {
				return updatedProduct
			}
		})
		} else {
			return 'Authentication failed.'
		}
		
	})
}


//Archive product
// module.exports.archiveProduct = (data) => {	
// 	return Product.findById(data.courseId).then((result, error) => {		
// 		if(data.isAdmin) {
// 			result.isActive = false

// 			console.log(result)
// 			return result.save().then((updatedCourse, error) => {
// 				if(error) {
// 					return false 					
// 				} else {
// 					return updatedCourse
// 				}
// 			})
// 		} else {
// 			return 'Not Admin.'
// 		}
// 	})
// }

//Using middleware
module.exports.archiveProduct = (data) => {	
	return Product.findById(data.productId).then((result, error) => {
		console.log(result)
		if(result.isActive) {
			result.isActive = false
				return result.save().then((updatedProduct, error) => {
					if(error) {
						return false 					
					} else {
						return updatedProduct
					}
				})
		} else {
			return 'Action Aborted. Archiving a product that has been archived already.'
		}
			
		
	})
}