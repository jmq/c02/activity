const Order = require('../models/Order')
const Product = require('../models/Product')
const mongoose = require('mongoose')

module.exports.checkout = async (data) => {	
	console.log(data)
	console.log(data.orderDetail, typeof(data.orderDetail))
	let newOrder =  await new Order ({			
		userId: data.userId,
		orderDetail: [{
			productId: data.productId,
			quantity: data.quantity,
			price: data.price,
			amount: data.quantity * data.price
		}],
		totalAmount: 100
	})

	return newOrder.save().then((order, error) => {
		if(error) {
			return false
		} else {
			return order
		}
	})
}

module.exports.checkoutMany = async (data) => {	
	let tempTotalAmount = 0
	let newOrderDetails = []

	let allProducts = await Product.find()

		// let currentTotal = 0
		// data.products.forEach(product => {

		// 	let foundProduct = allProducts.find((current) => current._id.toString() == product.productId)
		// 	currentTotal += foundProduct.price * product.amount
		// })

	await data.orderDetail.forEach(e => { 
		let foundProduct = allProducts.find((current) => current._id.toString() == e.productId)

		// let price = await Product.findById(e.productId).then(product => {			
		// 	console.log(`product price from forEach: ${product.price}`)
		// 	return product.price
		// })

		let price = foundProduct.price

		console.log(`price * quantity: ${price} * ${e.quantity}`)
		
		tempTotalAmount += price * e.quantity 
		console.log(`tempTotalAmount: ${tempTotalAmount}`)
		
		newOrderDetails.push({
			productId: e.productId,
			quantity: e.quantity,
			price: price,
			amount: price * e.quantity
		})
		console.log('after push')
		console.log(newOrderDetails)
	})	


	console.log(newOrderDetails)
	data.orderDetail = [...newOrderDetails]
	
	let newOrder =  await new Order ({	
		userId: data.userId,
		orderDetail: data.orderDetail,
		totalAmount: tempTotalAmount
	})

	console.log(newOrder)
	return newOrder
	// return newOrder.save().then((order, error) => {
	// 	if(error) {
	// 		return false
	// 	} else {
	// 		return order
	// 	}
	// })
}


//Get all order
module.exports.getAllOrders = () => {
	return Order.find({orderDetail: {$exists: true, $ne:[]}},{_id:0, __v:0}).then((result, error) => {
		console.log(result)
		return result
	})
}

//Get my order
module.exports.getMyOrders = (userId) => {
	return Order.find({userId: userId, orderDetail: {$exists: true, $ne:[]}},{_id:0, __v:0}).then((result, error) => {
		console.log(result)
		return result
	})
}







//Enroll
// module.exports.enroll = async (data) => {
// 	let isUserUpdated =  await user.findById(data.userId).then(user => {
// 		user.enrollments.push({courseId: data.courseId})
// 		return user.save().then((user, error) => {
// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})
// 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
// 		course.enrollees.push({userId: data.userId})
// 		return course.save().then((course, error) => {
// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})
// 	if(isUserUpdated && isCourseUpdated) {
// 		return true
// 	} else {
// 		return false
// 	}
// }
