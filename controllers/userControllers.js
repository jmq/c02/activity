const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')


//Get all users
module.exports.getUsers = () => {
	return User.find({}).then(result => {
		let x = result
		x.password = ''
		return x
	})
}

//User Regsitration
module.exports.registerUser = (requestBody) => {
	const newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10), //salt rounds:10
		isAdmin: requestBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			user.password = ''
			return user
		}
	})
}

//checkEmailExists check
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		return (result.length > 0) ? true : false
	})
}


//User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result === null) {
			return false
		} else {
			//compareSync(dataToBeCompared, enrcyptedData), boolean result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
				//return auth.createAccessToken(result)
			} else {
				return false
			}
		}
	})
}

//getUserDetails
// module.exports.getUserDetails = (reqBody) => {
// 	return user.findOne({id: reqBody._id}).then(result => {
// 		if(result === null) {
// 			return false
// 		} else {
// 			result.password = ''
// 			return result
// 		}
// 	})
// }


module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = ''
		return result		
	})
}

//Set as Admin Controller
module.exports.setAsAdmin = (userId) => {	
	return User.findById(userId.id).then((result, error) => {
			if(result.isAdmin) {
				return 'Action Aborted. Setting admin user to admin again.'
			} else {
				result.isAdmin = true
				return result.save().then((updatedUser, error) => {
					if(error) {
						return false 					
					} else {
						updatedUser.password = ''
						return updatedUser
					}
				})
			}
			
		
	})
}

//Checkout
// module.exports.checkout = async (data) => {
// 	let order = await User.findById(data.userId).then(user => {
// 		user.order.orderDetail.push({productId: data.productId})
// 		return user.save().then((user, error) => {
// 			return error ? false : true
// 		})
// 	})
// 	return order ? true : false
// }


//Enroll
// module.exports.enroll = async (data) => {
// 	let isUserUpdated =  await user.findById(data.userId).then(user => {
// 		user.enrollments.push({courseId: data.courseId})
// 		return user.save().then((user, error) => {
// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})
// 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
// 		course.enrollees.push({userId: data.userId})
// 		return course.save().then((course, error) => {
// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})
// 	if(isUserUpdated && isCourseUpdated) {
// 		return true
// 	} else {
// 		return false
// 	}
// }

// Controller for updating a specific task

// module.exports.completeTask = (taskID) => {
// 	//return Task.findByIdAndUpdate(task)
// 	return Task.findById(taskID).then((result, error) => {
// 		if(error) {
// 			console.log(error)
// 			return false
// 		} 
// 		result.status = 'complete'
// 		return result.save().then((updatedTask, saveErr) => {
// 			if(saveErr) {
// 				console.log(saveErr)
// 				return false
// 			} else {
// 				return updatedTask
// 			}
// 		})		
// 	})
// }
