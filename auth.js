const jwt = require('jsonwebtoken')
const secret = 'CourseBookingAPI'

//Create jsonWebToken
module.exports.createAccessToken = (user) => { 
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//Generate JSON web token using sign method	
	return jwt.sign(data, secret, {})
}

// Verify if user has logged in
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	if(typeof token !== 'undefined') {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return res.send({auth: 'failed'})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: 'failed'})
	}
}

// Decode
module.exports.decode = (token) => {
	if(typeof token !== 'undefined') {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null
			} else {
				//Send Payload
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}
}


//isAdmin
module.exports.isAdmin = (req, res, next) => {
	const isAdmin = this.decode(req.headers.authorization).isAdmin
	
	return isAdmin ? next() : res.send({auth: 'Auth failed.'})	
}

//notAdmin
module.exports.notAdmin = (req, res, next) => {
	const isAdmin = this.decode(req.headers.authorization).isAdmin
	
	return isAdmin ? res.send({auth: 'Only regular users can enroll'}) : next()
}