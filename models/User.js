const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({	
	email: {
		type: String,
		min: 8,
		required: true,
		lowercase: true,
		trim: true,
		unique: [true, 'email exists already. Please provide unique email']
	},
	password: {
		type: String,
		required: [true, 'Password is required! duh!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
}, {collection: 'Users'})

module.exports = mongoose.model('User', userSchema)

// enrollments: [{
// 		courseId: {
// 			type: String, 
// 			required: [true, 'User should be enrolled on at least 1 course.']
// 		},
// 		enrolledOn: {
// 			type: Date,
// 			default: () => new Date()
// 		},
// 		status: {
// 			type: String,
// 			default: 'Enrolled'
// 		}		
// 	}]