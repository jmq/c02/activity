const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is requred.'],		
	},
	purchasedOn: {
		type: Date,
		default: () => new Date()
	},
	orderDetail: {
		type: Array,
		default: []
	},
	// orderDetail: [{
	// 	productId: {
	// 		type: String,
	// 		//type: mongoose.SchemaTypes.ObjectId,
	// 		//ref: 'Product',
	// 		required: [true, 'Product Id is required.']
	// 	},
	// 	quantity: {
	// 		type: Number,
	// 		min: 1,
	// 		required: [true, 'At least 1 item is required']
	// 	},
	// 	price: {
	// 		type: Number,
	// 		required: true
	// 	},
	// 	amount: {
	// 		type: Number,
	// 		default: 0,
	// 		required: true
	// 	}
	// }],
		totalAmount: {
			type: Number,
			default: 0,
			required: true
		}
}, {collection: 'Orders'})


module.exports = mongoose.model('Order', orderSchema)

// ,
// 	order: {
// 		totalAmount: Number,
// 		purchasedOn: {
// 			type: Date,
// 			default: () => new Date()
// 		},
// 		orderDetail: [{
// 			productId: {
// 				type: mongoose.SchemaTypes.ObjectId,
// 				ref: 'Product'
// 			}
// 		}]
// 	}	