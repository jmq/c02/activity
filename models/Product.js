const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		min: 3,
		unique: [true, 'Duplicate item detected.'],
		required: [true, 'Course name is required.']
	},
	description: {
		type: String,
		required: [true, 'Description is required.']
	},
	price: {
		type: Number,
		min: 0,
		required: [true, 'Price is required.']		
	},
	isActive: {
		type: Boolean,
		required: true,
		default: true
	},
	createdOn: {
		type: Date,
		default: () => new Date()
	}
}, {collection: 'Products'})


module.exports = mongoose.model('Product', productSchema)