const express = require('express')
const userController = require('../controllers/orderControllers')
const auth = require('../auth')
const router = express.Router()

/*
* =====================================================================================
* ======================== P O S T  M E T H O D  O N L Y ==============================
* =====================================================================================
*/

//Route for User registration - admin only
router.post('/register', auth.verify, auth.isAdmin, (req, res) => {
	userController.registerUser(req.body).then(controllerRes => res.send(controllerRes))
})

//Check email if already exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(controllerRes => res.send(controllerRes))
})

//Check email if already exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(controllerRes => res.send(controllerRes))
})

//Routes for user authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(controllerRes => res.send(controllerRes))
})


/*
* =====================================================================================
* ========================= G E T  M E T H O D  O N L Y ===============================
* =====================================================================================
*/

//Route to GET all User
router.get('/', auth.verify, auth.isAdmin, (req, res) => {
	userController.getUsers().then(controllerRes => res.send(controllerRes))
})

//Routes for user details - require login
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({userId: userData.id}).then(controllerRes => res.send(controllerRes))
})


/*
* =====================================================================================
* ========================= P U T  M E T H O D  O N L Y ===============================
* =====================================================================================
*/

//Routes to set user as admin - require admin
router.put('/:userId/setAsAdmin', auth.verify, auth.isAdmin, (req, res) => {
	userController.setAsAdmin({id: req.params.userId}).then(controllerRes => res.send(controllerRes))
})



module.exports = router



//Enrollment - require login
// router.post('/enroll', auth.verify, auth.notAdmin, (req, res) => {
// 	const data = {
// 		//userId: req.body.userId,
// 		userId: auth.decode(req.headers.authorization).id,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController))
// })