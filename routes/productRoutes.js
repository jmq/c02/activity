const express = require('express')
const router = express.Router()
const productController = require('../controllers/productControllers')
const auth = require('../auth')

//Route for creating a product
router.post('/addProduct', auth.verify, auth.isAdmin, (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))	
})

//Route for retrieving all the courses
router.get('/all', auth.verify, (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})


//Route for retrieving all active courses
router.get('/active', (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})


//Retrieve specific course -by ID
router.get('/:productId', (req, res) => {
	productController.getProduct({id: req.params.productId}).then(resultFromController => res.send(resultFromController))
})

//Update Product
router.put('/:productId/update', auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		//isActive: req.body.isActive,
		updatedProduct: req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
})

//Archive course without using middleware - require login
router.put('/:productId/archive', auth.verify, auth.isAdmin, (req, res) => {
	const data = {
		productId: req.params.productId
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router
