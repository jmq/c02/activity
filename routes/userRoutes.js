const express = require('express')
const userController = require('../controllers/userControllers')
const orderController = require('../controllers/orderControllers')
const auth = require('../auth')
const router = express.Router()

/*
* =====================================================================================
* ======================== P O S T  M E T H O D  O N L Y ==============================
* =====================================================================================
*/

//Route for User registration - admin only
router.post('/register', auth.verify, auth.isAdmin, (req, res) => {
	userController.registerUser(req.body).then(controllerRes => res.send(controllerRes))
})

//Check email if already exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(controllerRes => res.send(controllerRes))
})

//Check email if already exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(controllerRes => res.send(controllerRes))
})

//Routes for user authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(controllerRes => res.send(controllerRes))
})

//=================================== O R D E R S =====================================
//Checkout - require login of not an admin user
router.post('/checkout', auth.verify, auth.notAdmin, (req, res) => {
	console.log(req.body)
	console.log('end of req body')
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		price: req.body.price,
		amount: req.body.amount,		
		totalAmount: req.body.totalAmount
	}
	orderController.checkout(data).then(controllerRes => res.send(controllerRes))
})

//Checkout - require login of not an admin user
router.post('/checkoutMany', auth.verify, auth.notAdmin, (req, res) => {
	console.log(req.body)
	console.log('end of req body')
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		// Spread Array Literals
		orderDetail : [...req.body.orderDetail]
	}
	orderController.checkoutMany(data).then(controllerRes => res.send(controllerRes))
})



/*
* =====================================================================================
* ========================= G E T  M E T H O D  O N L Y ===============================
* =====================================================================================
*/

//Route to GET all User
router.get('/', auth.verify, auth.isAdmin, (req, res) => {
	userController.getUsers().then(controllerRes => res.send(controllerRes))
})

//Routes for user details - require login
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({userId: userData.id}).then(controllerRes => res.send(controllerRes))
})

//Retrieve All Orders (Admin only)
router.get('/allOrders', auth.verify, auth.isAdmin, (req, res) => {
	//const userId = auth.decode(req.headers.authorization).id
	orderController.getAllOrders().then(controllerRes => res.send(controllerRes))
})

//Retrieve Authenticated User's Orders (NON-admin only)
router.get('/myOrders', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id
	orderController.getMyOrders(userId).then(controllerRes => res.send(controllerRes))
})



/*
* =====================================================================================
* ========================= P U T  M E T H O D  O N L Y ===============================
* =====================================================================================
*/

//Routes to set user as admin - require admin
router.put('/:userId/setAsAdmin', auth.verify, auth.isAdmin, (req, res) => {
	userController.setAsAdmin({id: req.params.userId}).then(controllerRes => res.send(controllerRes))
})



module.exports = router



//Enrollment - require login
// router.post('/enroll', auth.verify, auth.notAdmin, (req, res) => {
// 	const data = {
// 		//userId: req.body.userId,
// 		userId: auth.decode(req.headers.authorization).id,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController))
// })